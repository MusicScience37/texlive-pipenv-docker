# texlive-pipenv-docker

[![pipeline status](https://gitlab.com/MusicScience37Projects/docker/texlive-pipenv-docker/badges/main/pipeline.svg)](https://gitlab.com/MusicScience37Projects/docker/texlive-pipenv-docker/-/commits/main)

Docker image of TeXLive + Pipenv.

## Container Registries

You can pull automatically built images from following registries:

- [GitLab Container Registry](https://gitlab.com/MusicScience37Projects/docker/texlive-pipenv-docker/container_registry)
  - latest stable image: `registry.gitlab.com/musicscience37projects/docker/texlive-pipenv-docker:latest`

## Repositories

- [GitLab](https://gitlab.com/MusicScience37Projects/docker/texlive-pipenv-docker):
  for development including CI

## Testing

For test of this project,
use `./tool.py test` command.
